package cl.udp.tesis.scrum.dashboard.dal.helper;

import cl.udp.tesis.scrum.dashboard.dal.dto.VelocityDTO;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.entity.Velocidad;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EntityToDtoTest {

    @InjectMocks
    EntityToDto entityToDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(entityToDto);
    }

    @Test
    void shouldConvertVelocidadToVelocityDTO() {
        Velocidad velocidad = new Velocidad();
        Sprint sprint = new Sprint();
        sprint.setNombreSprint("sprint 1");
        velocidad.setSprint(sprint);
        velocidad.setIdVelocidad(1);
        velocidad.setPHistoriaRealizado(23);
        velocidad.setPHistoriaComprometido(45);

        VelocityDTO velocityDTO = entityToDto.velocityEntityToDTO(velocidad);

        Assertions.assertEquals(sprint.getNombreSprint(), velocityDTO.getSprint());

    }
}
