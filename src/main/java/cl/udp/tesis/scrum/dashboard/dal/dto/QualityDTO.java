package cl.udp.tesis.scrum.dashboard.dal.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class QualityDTO {

    private String sprint;
    private int TechnicalDebtEstimate;

}
