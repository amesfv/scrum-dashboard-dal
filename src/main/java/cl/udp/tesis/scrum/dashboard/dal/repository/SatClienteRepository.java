package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.SatCliente;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SatClienteRepository extends JpaRepository<SatCliente, Integer>, JpaSpecificationExecutor<SatCliente> {

    SatCliente findBySprint(Sprint sprint);
}