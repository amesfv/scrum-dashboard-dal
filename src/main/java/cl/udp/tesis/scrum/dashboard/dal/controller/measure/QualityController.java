package cl.udp.tesis.scrum.dashboard.dal.controller.measure;

import cl.udp.tesis.scrum.dashboard.dal.entity.Calidad;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.helper.EntityToDto;
import cl.udp.tesis.scrum.dashboard.dal.repository.CalidadRepository;
import cl.udp.tesis.scrum.dashboard.dal.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/quality")
public class QualityController {

    @Autowired
    CalidadRepository repository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    EntityToDto entityToDto;

    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("sprintId") int sprintId,
                                       @RequestParam("technicalDeb") int technicalDeb){
        Calidad calidad = new Calidad();
        Sprint sprint = new Sprint();
        sprint.setIdSprint(sprintId);
        calidad.setSprint(sprint);
        calidad.setDeudaTecnica(technicalDeb);
        repository.save(calidad);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll(){
        return new ResponseEntity<>(repository.findAll(), OK);
    }

    @GetMapping("/findbyid/{sprintId}")
    public ResponseEntity<Object> getById(@PathVariable("sprintId") int sprintId){
        Sprint sprint = sprintRepository.getOne(sprintId);
        return new ResponseEntity<>(entityToDto.calidadToQualityDTO(repository.findBySprint(sprint)), OK);
    }
}
