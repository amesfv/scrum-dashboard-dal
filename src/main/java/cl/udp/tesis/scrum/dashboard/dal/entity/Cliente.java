package cl.udp.tesis.scrum.dashboard.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CLIENTE", insertable = false, nullable = false)
    private Integer idCliente;

    @Column(name = "NOMBRE_CLIENTE")
    private String nombreCliente;

    
}