package cl.udp.tesis.scrum.dashboard.dal.helper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Helper {


    public static LocalDate StringToLocalDate(String date){
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }

}
