package cl.udp.tesis.scrum.dashboard.dal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "velocidad")
@Entity
@Data
public class Velocidad implements Serializable {
    private static final long serialVersionUID = 1L;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_SPRINT")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Sprint sprint;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_VELOCIDAD", insertable = false, nullable = false)
    private Integer idVelocidad;

    @Column(name = "P_HISTORIA_COMPROMETIDO")
    private Integer pHistoriaComprometido;

    @Column(name = "P_HISTORIA_REALIZADO")
    private Integer pHistoriaRealizado;

    
}