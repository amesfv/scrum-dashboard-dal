package cl.udp.tesis.scrum.dashboard.dal.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HappinessDTO {

    private String sprint;
    private float happinessRating;

}
