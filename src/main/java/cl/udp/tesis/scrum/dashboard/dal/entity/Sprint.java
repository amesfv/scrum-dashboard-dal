package cl.udp.tesis.scrum.dashboard.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Table(name = "sprint")
@Data
@Entity
public class Sprint implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "FECHA_INICIO")
    private LocalDate fechaInicio;

    @Column(name = "FECHA_TERMINO")
    private LocalDate fechaTermino;

    @ManyToOne()
    @JoinColumn(name = "ID_EQUIPO")
    private Equipo equipo;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SPRINT", insertable = false, nullable = false)
    private Integer idSprint;

    @Column(name = "NOMBRE_SPRINT")
    private String nombreSprint;

}