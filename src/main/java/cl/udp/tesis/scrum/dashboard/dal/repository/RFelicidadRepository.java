package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.RFelicidad;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RFelicidadRepository extends JpaRepository<RFelicidad, Integer>, JpaSpecificationExecutor<RFelicidad> {

    RFelicidad findBySprint(Sprint idSprint);
}