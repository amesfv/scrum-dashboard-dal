package cl.udp.tesis.scrum.dashboard.dal.helper;

import cl.udp.tesis.scrum.dashboard.dal.dto.*;
import cl.udp.tesis.scrum.dashboard.dal.entity.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

@Component
public class EntityToDto {

    ModelMapper modelMapper;

    public VelocityDTO velocityEntityToDTO(Velocidad velocidad){
        modelMapper = new ModelMapper();
        TypeMap<Velocidad, VelocityDTO> typeMap = modelMapper.createTypeMap(Velocidad.class,VelocityDTO.class);
        typeMap.addMappings(mapper -> mapper.map(src -> src.getSprint().getNombreSprint(),VelocityDTO::setSprint));
        typeMap.addMapping(Velocidad::getPHistoriaRealizado, VelocityDTO::setResolvedStoryPoints);
        typeMap.addMapping(Velocidad::getPHistoriaComprometido, VelocityDTO::setCompromisedStoryPoints);
        return modelMapper.map(velocidad,VelocityDTO.class);
    }

   public SatisfactionDTO satClienteToSatiscationDTO(SatCliente satCliente){
        modelMapper = new ModelMapper();
        TypeMap<SatCliente, SatisfactionDTO> typeMap = modelMapper.createTypeMap(SatCliente.class,SatisfactionDTO.class);
        typeMap.addMappings(mapper -> mapper.map(src -> src.getSprint().getNombreSprint(),SatisfactionDTO::setSprint));
        typeMap.addMapping(SatCliente::getIndiceSatisfaccion, SatisfactionDTO::setSatisfactionRating);
        return modelMapper.map(satCliente,SatisfactionDTO.class);
    }

    public QualityDTO calidadToQualityDTO(Calidad calidad){
        modelMapper = new ModelMapper();
        TypeMap<Calidad, QualityDTO> typeMap = modelMapper.createTypeMap(Calidad.class, QualityDTO.class);
        typeMap.addMappings(mapping -> mapping.map(src -> src.getSprint().getNombreSprint(), QualityDTO::setSprint));
        typeMap.addMapping(Calidad::getDeudaTecnica, QualityDTO::setTechnicalDebtEstimate);
        return  modelMapper.map(calidad,QualityDTO.class);
    }

    public HappinessDTO rFelicidadToHappinessDTO(RFelicidad rFelicidad){
        modelMapper = new ModelMapper();
        TypeMap<RFelicidad, HappinessDTO> typeMap = modelMapper.createTypeMap(RFelicidad.class, HappinessDTO.class);
        typeMap.addMappings(mapping -> mapping.map(src -> src.getSprint().getNombreSprint(), HappinessDTO::setSprint));
        typeMap.addMapping(RFelicidad::getIndiceFelicidad, HappinessDTO::setHappinessRating);
        return  modelMapper.map(rFelicidad,HappinessDTO.class);
    }

    public SprintDTO sprintToSprintDTO(Sprint sprint){
        modelMapper = new ModelMapper();
        TypeMap<Sprint, SprintDTO> typeMap = modelMapper.createTypeMap(Sprint.class,SprintDTO.class);
        typeMap.addMappings(mapping -> mapping.map(src-> src.getEquipo().getNombreEquipo(), SprintDTO::setTeamName));
        typeMap.addMapping(Sprint::getNombreSprint,SprintDTO::setSprintName);
        typeMap.addMapping(Sprint::getFechaInicio,SprintDTO::setInitDate);
        typeMap.addMapping(Sprint::getFechaTermino,SprintDTO::setEndDate);
        return modelMapper.map(sprint,SprintDTO.class);
    }

}
