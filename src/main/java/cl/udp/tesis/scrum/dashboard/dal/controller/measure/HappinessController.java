package cl.udp.tesis.scrum.dashboard.dal.controller.measure;

import cl.udp.tesis.scrum.dashboard.dal.entity.RFelicidad;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.helper.EntityToDto;
import cl.udp.tesis.scrum.dashboard.dal.repository.RFelicidadRepository;
import cl.udp.tesis.scrum.dashboard.dal.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/happiness")
public class HappinessController {

    @Autowired
    RFelicidadRepository repository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    EntityToDto entityToDto;

    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("sprintId") int sprintId,
                                       @RequestParam("happinessMean") float happinessMean){
        RFelicidad felicidad = new RFelicidad();
        Sprint sprint = new Sprint();
        sprint.setIdSprint(sprintId);
        felicidad.setSprint(sprint);
        felicidad.setIndiceFelicidad(happinessMean);
        repository.save(felicidad);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll(){
        return new ResponseEntity<>(repository.findAll(), OK);
    }

    @GetMapping("/findbyid/{sprintId}")
    public ResponseEntity<Object> getById(@PathVariable("sprintId") int sprintId){
        Sprint sprint = sprintRepository.getOne(sprintId);
        return new ResponseEntity<>(entityToDto.rFelicidadToHappinessDTO(repository.findBySprint(sprint)), OK);
    }
}
