package cl.udp.tesis.scrum.dashboard.dal.controller;

import cl.udp.tesis.scrum.dashboard.dal.entity.Equipo;
import cl.udp.tesis.scrum.dashboard.dal.repository.EquipoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/team")
public class TeamController {

    @Autowired
    EquipoRepository repository;

    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("teamName") String teamName,
                                           @RequestParam("clientId") int clientId){
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo(teamName);
        equipo.setIdCliente(clientId);
        repository.save(equipo);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll(){
        return new ResponseEntity<>(repository.findAll(), OK);
    }
}
