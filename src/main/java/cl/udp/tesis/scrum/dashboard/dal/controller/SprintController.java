package cl.udp.tesis.scrum.dashboard.dal.controller;

import cl.udp.tesis.scrum.dashboard.dal.dto.SprintDTO;
import cl.udp.tesis.scrum.dashboard.dal.entity.Equipo;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.entity.Velocidad;
import cl.udp.tesis.scrum.dashboard.dal.helper.EntityToDto;
import cl.udp.tesis.scrum.dashboard.dal.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static cl.udp.tesis.scrum.dashboard.dal.helper.Helper.StringToLocalDate;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/sprint")
public class SprintController {

    @Autowired
    SprintRepository repository;

    @Autowired
    EntityToDto entityToDto;


    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("sprintName") String sprintName,
                                       @RequestParam("initDate") String initDate,
                                       @RequestParam("endDate") String endDate,
                                       @RequestParam("teamId") int teamId) {
        Sprint sprint = new Sprint();
        Equipo equipo = new Equipo();
        equipo.setIdEquipo(teamId);
        sprint.setNombreSprint(sprintName);
        sprint.setFechaInicio(StringToLocalDate(initDate));
        sprint.setFechaTermino(StringToLocalDate(endDate));
        sprint.setEquipo(equipo);
        repository.save(sprint);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll() {
        return new ResponseEntity<>(repository.findAll(), OK);
    }

    @GetMapping("/findbyid/{teamId}/{sprints}")
    public ResponseEntity<List<SprintDTO>> getByTeamId(@PathVariable("teamId") int teamId,
                                                       @PathVariable("sprints") int sprints) {
        List<Sprint> sprintsByTeam = repository.findLastNSprintByTeam(teamId, sprints);
        List<SprintDTO> sprintDTOS = sprintsByTeam.stream().map(sprint -> entityToDto.sprintToSprintDTO(sprint)).collect(Collectors.toList());
        return new ResponseEntity<>(sprintDTOS, OK);
    }

}
