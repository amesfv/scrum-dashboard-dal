package cl.udp.tesis.scrum.dashboard.dal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "sat_cliente")
public class SatCliente implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SATISFACCION", insertable = false, nullable = false)
    private Integer idSatisfaccion;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_SPRINT")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Sprint sprint;

    @Column(name = "INDICE_SATISFACCION")
    private Float indiceSatisfaccion;

    
}