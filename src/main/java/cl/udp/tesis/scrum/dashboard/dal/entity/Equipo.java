package cl.udp.tesis.scrum.dashboard.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "equipo")
@Entity
@Data
public class Equipo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "ID_CLIENTE")
    private Integer idCliente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EQUIPO", insertable = false, nullable = false)
    private Integer idEquipo;

    @Column(name = "NOMBRE_EQUIPO")
    private String nombreEquipo;

    
}