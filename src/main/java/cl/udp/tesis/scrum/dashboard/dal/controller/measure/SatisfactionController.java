package cl.udp.tesis.scrum.dashboard.dal.controller.measure;

import cl.udp.tesis.scrum.dashboard.dal.entity.SatCliente;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.helper.EntityToDto;
import cl.udp.tesis.scrum.dashboard.dal.repository.SatClienteRepository;
import cl.udp.tesis.scrum.dashboard.dal.repository.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/satisfaction")
public class SatisfactionController {

    @Autowired
    SatClienteRepository repository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    EntityToDto entityToDto;

    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("sprintId") int sprintId,
                                       @RequestParam("satisfactionMean") float satisfactionMean){
        SatCliente satCliente = new SatCliente();
        Sprint sprint = new Sprint();
        sprint.setIdSprint(sprintId);
        satCliente.setSprint(sprint);
        satCliente.setIndiceSatisfaccion(satisfactionMean);
        repository.save(satCliente);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll(){
        return new ResponseEntity<>(repository.findAll(), OK);
    }

    @GetMapping("/findbyid/{sprintId}")
    public ResponseEntity<Object> getById(@PathVariable("sprintId") int sprintId){
        Sprint sprint = sprintRepository.getOne(sprintId);
        return new ResponseEntity<>(entityToDto.satClienteToSatiscationDTO(repository.findBySprint(sprint)), OK);
    }
}
