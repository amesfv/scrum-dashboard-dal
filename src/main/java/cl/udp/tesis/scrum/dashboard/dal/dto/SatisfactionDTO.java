package cl.udp.tesis.scrum.dashboard.dal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SatisfactionDTO {

    private String sprint;
    private float satisfactionRating;

}
