package cl.udp.tesis.scrum.dashboard.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "usuario")
@Data
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "CONTRASENA")
    private String CONTRASENA;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "ID_USUARIO", nullable = false)
    private Integer idUsuario;

    @Column(name = "NOMBRE")
    private String NOMBRE;

    
}