package cl.udp.tesis.scrum.dashboard.dal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"cl.udp.tesis.scrum.dashboard.dal.entity"})
public class DalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DalApplication.class, args);
	}

}
