package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.Calidad;
import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CalidadRepository extends JpaRepository<Calidad, Integer>, JpaSpecificationExecutor<Calidad> {

    Calidad findBySprint(Sprint sprint);
}