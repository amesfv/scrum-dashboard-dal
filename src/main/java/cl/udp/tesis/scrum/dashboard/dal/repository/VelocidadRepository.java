package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.entity.Velocidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VelocidadRepository extends JpaRepository<Velocidad, Integer>, JpaSpecificationExecutor<Velocidad> {

    Velocidad findBySprint(Sprint sprint);
}