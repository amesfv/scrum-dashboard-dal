package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>, JpaSpecificationExecutor<Cliente> {

}