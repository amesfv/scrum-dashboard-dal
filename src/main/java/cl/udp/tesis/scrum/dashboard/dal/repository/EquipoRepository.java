package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.Equipo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EquipoRepository extends JpaRepository<Equipo, Integer>, JpaSpecificationExecutor<Equipo> {

}