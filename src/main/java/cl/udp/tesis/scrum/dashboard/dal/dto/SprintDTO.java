package cl.udp.tesis.scrum.dashboard.dal.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SprintDTO {

    private LocalDate initDate;
    private LocalDate endDate;
    private String teamName;
    private String sprintName;
    private Integer idSprint;

}
