package cl.udp.tesis.scrum.dashboard.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "proyecto")
public class Proyecto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "ID_CLIENTE")
    private Integer idCliente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PROYECTO", insertable = false, nullable = false)
    private Integer idProyecto;

    @Column(name = "NOMBRE_PROYECTO")
    private String nombreProyecto;

    
}