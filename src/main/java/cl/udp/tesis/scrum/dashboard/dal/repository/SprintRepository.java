package cl.udp.tesis.scrum.dashboard.dal.repository;

import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
        import org.springframework.data.jpa.repository.Query;

        import java.util.List;

public interface SprintRepository extends JpaRepository<Sprint, Integer>, JpaSpecificationExecutor<Sprint> {

    @Query(value="SELECT * FROM sprint s WHERE s.ID_EQUIPO = :teamId ORDER BY s.ID_SPRINT DESC LIMIT :sprints", nativeQuery = true)
    List<Sprint> findLastNSprintByTeam(int teamId,int sprints);
}