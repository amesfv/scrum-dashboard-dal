package cl.udp.tesis.scrum.dashboard.dal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "calidad")
@Entity
@Data
public class Calidad implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "DEUDA_TECNICA")
    private Integer deudaTecnica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CALIDAD", insertable = false, nullable = false)
    private Integer idCalidad;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_SPRINT")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Sprint sprint;

    
}