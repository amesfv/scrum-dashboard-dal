package cl.udp.tesis.scrum.dashboard.dal.controller;

import cl.udp.tesis.scrum.dashboard.dal.entity.Cliente;
import cl.udp.tesis.scrum.dashboard.dal.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/client")
public class ClientController {

    @Autowired
    ClienteRepository repository;

    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("clientName") String clientName){
        Cliente cliente = new Cliente();
        cliente.setNombreCliente(clientName);
        repository.save(cliente);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll(){
        return new ResponseEntity<>(repository.findAll(), OK);
    }

}
