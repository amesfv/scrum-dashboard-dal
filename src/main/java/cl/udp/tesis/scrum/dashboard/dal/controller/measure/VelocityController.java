package cl.udp.tesis.scrum.dashboard.dal.controller.measure;

import cl.udp.tesis.scrum.dashboard.dal.entity.Sprint;
import cl.udp.tesis.scrum.dashboard.dal.entity.Velocidad;
import cl.udp.tesis.scrum.dashboard.dal.helper.EntityToDto;
import cl.udp.tesis.scrum.dashboard.dal.repository.SprintRepository;
import cl.udp.tesis.scrum.dashboard.dal.repository.VelocidadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/dal/velocity")
public class VelocityController {

    @Autowired
    VelocidadRepository repository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    EntityToDto entityToDto;

    @PostMapping("/add")
    public ResponseEntity<String> save(@RequestParam("sprintId") int sprintId,
                                           @RequestParam("compromisedStoryPoints") int compromisedStoryPoints,
                                           @RequestParam("resolvedStoryPoints") int resolvedStoryPoints){
        Velocidad velocidad = new Velocidad();
        Sprint sprint = new Sprint();
        sprint.setIdSprint(sprintId);
        velocidad.setSprint(sprint);
        velocidad.setPHistoriaComprometido(compromisedStoryPoints);
        velocidad.setPHistoriaRealizado(resolvedStoryPoints);
        repository.save(velocidad);
        return new ResponseEntity<>("success", CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getAll(){
        return new ResponseEntity<>(repository.findAll(), OK);
    }

    @GetMapping("/findbyid/{sprintId}")
    public ResponseEntity<Object> getById(@PathVariable("sprintId") int sprintId){
        Sprint sprint = sprintRepository.getOne(sprintId);
        return new ResponseEntity<>(entityToDto.velocityEntityToDTO(repository.findBySprint(sprint)), OK);
    }
}
